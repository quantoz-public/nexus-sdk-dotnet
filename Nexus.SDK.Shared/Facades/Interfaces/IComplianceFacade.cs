﻿namespace Nexus.SDK.Shared.Facades.Interfaces
{
    public interface IComplianceFacade
    {
        ITrustLevelsFacade Trustlevels { get; }
    }
}
