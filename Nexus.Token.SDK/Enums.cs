﻿namespace Nexus.Token.SDK;

public enum PaymentMethodType
{
    Funding,
    Payout
}
