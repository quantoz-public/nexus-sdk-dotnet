# Nexus SDK Repository (Moved to GitHub)

⚠️ **Important Notice:** This repository has moved to GitHub!

**New GitHub Repository:** [Link to the GitHub Repository](https://github.com/QuantozTechnology/Nexus.Sdk.Dotnet)

We have decided to continue the development of this project on GitHub. All future updates and contributions will be made there. 
